using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShootWeaponController : WeaponController
{
    private ShootWeaponData _shootWeaponData;
    protected override void Start()
    {
        base.Start();
        _shootWeaponData = (ShootWeaponData)WeaponData;
    }
    public override void Attack()
    {
        Debug.Log("Shoot");
    }
}
