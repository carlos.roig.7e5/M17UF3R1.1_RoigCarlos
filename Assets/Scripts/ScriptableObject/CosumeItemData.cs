using UnityEngine;

[CreateAssetMenu(fileName = "ConsumeItemData", menuName = "ConsumeItemData")]
public class CosumeItemData : ItemData
{
    public int ActualQuantity;
    public int MaxQuantity;
    //public CosumeItemController Script;
}
