using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "DamageItemData", menuName = "DamageItemData")]
public class DamageItem : ItemData
{
    public float Damage;
}
